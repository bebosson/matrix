use vector::vector::Vector;
use matrix_lib::matrix::Matrix;


fn main() {
    let mut u1 = Vector::from(&[1, 2, 3]);
    let mut v1 = Vector::from(&[4, 5, 6]);
    let mut w1 = Vector::from(&[7, 8, 9]);
    let mut u2 = Vector::from(&[1, 2, 3]);
    let mut v2 = Vector::from(&[4, 5, 6]);
    let mut w2 = Vector::from(&[7, 8, 9]);
    let mut matrice1 = Matrix::from(&[u1,v1,w1]);
    let mut matrice2 = Matrix::from(&[u2,v2,w2]);
    
    matrice1.add(&matrice2);
    // matrice1.print();
    // u2.scl(2.).print();
}