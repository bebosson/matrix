use matrix_lib::matrix::{Matrix as Matrix, Determinant};
use vector::vector::Vector as Vector;
use matrix_lib::matrix::Rank;


fn main() {
    let A0 = Vector::from(&[1., 1., 2.]);
    let A1 = Vector::from(&[1., 2., 1.]);
    let A2 = Vector::from(&[2., 1., 1.]);


    let A0 = Vector::from(&[8., 5., -2.]);
    let A1 = Vector::from(&[4., 7., 20.]);
    let A2 = Vector::from(&[7., 6., 1.]);

    let F0 = Vector::from(&[2., 8., 4.,2.]);
    let F1 = Vector::from(&[2., 5., 1.,5.]);
    let F2 = Vector::from(&[4., 10., -1.,1.]);
    let C0 = Vector::from(&[1., 2., 3.,4.]);
    let C1 = Vector::from(&[2., 5., 6.,8.]);
    let C2 = Vector::from(&[3., 4., 10.,12.]);
    let D0 = Vector::from(&[2., 4., 1.,3.]);
    let D1 = Vector::from(&[6., 2., 3.,9.]);
    let D2 = Vector::from(&[1., 1., 1.,1.]);
    let E0 = Vector::from(&[1., -3., 0.,-5.]);
    let E1 = Vector::from(&[-3., 7., 0.,9.]);
    let G0 = Vector::from(&[1., 3., 6.,2.,1.]);
    let G1 = Vector::from(&[0., 1., 5.,2.,6.]);
    let G2 = Vector::from(&[0., 0., 0.,0.,-2.]);
    let G3 = Vector::from(&[0., 0., 0.,0.,0.]);
    let H0 = Vector::from(&[1., 2., 3.,6.]);
    let H1 = Vector::from(&[2., -3., 2.,14.]);
    let H2 = Vector::from(&[3., 1., -1.,-2.]);
    let I0 = Vector::from(&[1., 1.]);
    let I1 = Vector::from(&[1., 1.]);
    // let I2 = Vector::from(&[4., 4.]);
    // let I3 = Vector::from(&[2., 2.]);
    let J1 = Vector::from(&[1., 2., 0., 0.]);
    let J2 = Vector::from(&[2., 4., 0., 0.]);
    let J3 = Vector::from(&[1., 2., 1., 1.]);

    let mut A = Matrix::from(&[A0, A1, A2]);
    let mut F = Matrix::from(&[F0, F1, F2]);
    let mut C = Matrix::from(&[C0, C1, C2]);
    let mut D = Matrix::from(&[D0, D1, D2]);
    let mut E = Matrix::from(&[E0, E1]);
    let mut G = Matrix::from(&[G0, G1, G2, G3]);
    let mut H = Matrix::from(&[H0, H1, H2]);
    // let mut I = Matrix::from(&[I0, I1, I2, I3]);
    let mut I = Matrix::from(&[I0, I1]);
    let mut J = Matrix::from(&[J1, J2, J3]);

    println!("{}", A.rank());
    println!("{}", J.rank());

}
