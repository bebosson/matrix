pub mod matrix {

    use std::clone::Clone;
    use std::fmt::Debug;
    use std::ops::Add;
    use std::ops::Not;
    use std::ops::Sub;
    use std::ops::Mul;
    use std::cmp::PartialOrd;
    use vector::vector::Vector as Vector;
    use vector::vector::Dot;
    use vector::vector::*;

    #[derive(Debug)]
    pub struct Matrix<K> //generic type
    {
        pub coor: Vec<Vector<K>>, //row vector of the matrix 
        pub n: usize, //nombre de vecteurs (ligne, row)
        pub m : usize, //dimension des vecteurs (colonne, column)
    }


    impl<K> Matrix<K> //traits
    where K: Copy + Debug + Add<Output = K> + Sub<Output = K> + Mul<Output = K>,
    {

        pub fn from(v : &[Vector<K>]) -> Self 
        {
            if v.len() != 0
            {
                let mut i:usize = 1;
                while i < v.len()
                {
                    if v[i].m != v[i-1].m
                    {
                        panic!("origin vector matrix needs to have the same dimension")
                    }
                    i += 1;
                }
            }
            Matrix{
                coor: v.to_vec(),
                n : v.len(),
                m : v[0].m,
            }
        }

        pub fn new(n: usize, m: usize) -> Matrix<K>
        {    
            let mut w = Matrix{
                coor: Vec::with_capacity(n),
                n: n,
                m: m,
            };
            for i in 0..w.n
            {
                w.coor.push(Vector::new(w.n));
            }
            return w;
        }

        pub fn new_one(n: usize, m: usize) -> Matrix<K>
        {    
            let mut w = Matrix{
                coor: Vec::with_capacity(n),
                n: n,
                m: m,
            };
            return w;
        }

        pub fn add(&mut self, v: &Matrix<K>){
            if self.m != v.m || self.n != v.n {  panic!("undifined result!");}
            let mut i:usize = 0;
            let mut j:usize = 0;
            while i < self.m
            {
                j = 0;
                while j < self.n
                {
                    self.coor[i].coor[j] = self.coor[i].coor[j] + v.coor[i].coor[j];
                    j += 1;
                }
                i += 1;
            }
        }

        pub fn sub(&mut self, v: &Matrix<K>)
        {
            if self.m != v.m || self.n != v.n {  panic!("undifined result!");}
            let mut i:usize = 0;
            let mut j:usize = 0;
            while i < self.m
            {
                j = 0;
                while j < self.n
                {
                    self.coor[i].coor[j] = self.coor[i].coor[j] - v.coor[i].coor[j];
                    j += 1;
                }
                i += 1;
            }
        }

        pub fn scl(&mut self, a: K)
        {
            let mut i:usize = 0;
            let mut j:usize = 0;
            while i < self.m
            {
                j = 0;
                while j < self.n
                {
                    self.coor[i].coor[j] = self.coor[i].coor[j] * a;
                    j += 1;
                }
                i += 1;
            }
        }

        pub fn copy(&mut self, v: &Matrix<K>, list_index_i: &Vec<usize>, list_index_j: &Vec<usize>, list_index_i_v: &Vec<usize>, list_index_j_v: &Vec<usize>)
        {
            let mut i_v: usize = 0;
            let mut j_v: usize = 0;

            for i in list_index_i
            {
                for j in list_index_j
                {
                    self.coor[*i].coor[*j] = v.coor[list_index_i_v[i_v]].coor[list_index_j_v[j_v]];
                    j_v += 1;
                }
                j_v = 0;
                i_v += 1;
            }
        }

        pub fn print(&self)
        {
            for i in 0..self.n
            {
                self.coor[i].print();
            }
            print!("------\n");
        }

        pub fn gimmecolumn(&self, m: usize) -> Vector<K>{
            if self.m < m
            {
                panic!("origin vector matrix needs to have the same dimension")
            }
            let mut v: Vector<K> = Vector::new(self.n);
            for i in 0..self.n{
                v.coor.push(self.coor[i].coor[m]);
            }
            v
        }

        pub fn mul_vec(&mut self, vec: Vector<K>) -> Vector<K>
        where K: Max + Pow + Abs + Copy + Debug + PartialEq + PartialOrd,
        {
            
            /*
                Soit le produit de A(self) une matrice dans R(n x m)
                et B(vec) le vecteur de taille n
                le produit AxB(C) est un vecteur de taille m'
                ici m(dimension des vecteurs de A) doit etre egal a m' (dimension du vecteur B)
                securite que l'on retrouve dans le dot_product que l'on dupliquera ici  
            */
            if self.m != vec.m {panic!("le vecteur et les vecteurs de la matrice doivent avoir la meme dimension")}
            else {
                let mut v = Vector::new(self.n);
                for i in 0..self.n
                {
                    v.coor.push(self.coor[i].dot(&vec));
                }
                v
            }
        }

        pub fn mul_mat(&mut self, mat: Matrix<K>) -> Matrix<K>
        where K: Max + Pow + Abs + Copy + Debug + PartialEq + PartialOrd,
        {
            /* soit le produit de A(self) par B(mat)
             avec A app R(n x m)
             et B app R(n' x m')
             (avec n et n': nombre de vecteurs, row)
             (m et m': dimension des vecteurs, column)
             on declare C tel que C = A x B
             avec C app R(n x m') n = self.n et m = mat.m 
            */
            if self.m != mat.n
            {
                panic!("produit matricielle non defini");
            }
            let mut c: Matrix<K>;
            c = Matrix::<K>::new(self.n, mat.m);    // on cree C, le resultat du produit
            for i in 0..self.n              //parcours les vecteurs de self
            {
                for j in 0..mat.m           //parcours les columns de mat 
                {
                    // chaque element de v est le resultat du dot.product 
                    //des vecteurs row de self et des vecteurs column de mat
                    c.coor[i].coor.push(self.coor[i].dot(&mat.gimmecolumn(j)));
                } 
            }
            c
        }

        pub fn trace(&mut self) -> K
        where K: Add<Output = K>,
        {
            let mut sum: K;
            if self.m != self.n && self.m == 0{panic!("la matrice n'est pas carree et non nulle")}
            else{
                sum = self.coor[0].coor[0];
                for i in 1..self.n
                {
                    sum = sum + self.coor[i].coor[i];
                }
                sum
            }
        }

        pub fn transpose(&mut self) -> Matrix<K>
        where K: Max + Pow + Abs + Copy + Debug + PartialEq + PartialOrd,{
            let mut mat = Matrix::new(self.m,self.n);
            for i in 0..self.m{
                for j in 0..self.n{
                    mat.coor[i].coor.push(self.gimmecolumn(i).coor[j]);
                }
            }
            mat
        }

       
    }

    pub trait Inv{
        fn inv(&self) -> f32;
    }
    
    impl Inv for f32{
        fn inv(&self) -> f32{
            if *self == 0. { panic!("div by zero!")}
            1. / self
        }
    }
    
    pub trait CompareK{
        fn compare_f(&self, u: f32) -> i8;
    }
    
    impl CompareK for f32{
        fn compare_f(&self, u: f32) -> i8{
            if *self == u
            {
                1
            }
            else {
                0
            }
                            
        }
    }

    pub trait Rowechelon<K>{
        fn row_echelon(&mut self);
    }

    fn rot_one(t: & mut Vec<usize>, max: usize)
    {
        for i in &mut *t{
            *i += 1;
            if *i > max - 1
            {
                *i = 0;
            }
        }
    
    }

    fn inc_indice(j: &mut usize, tab: &mut Vec<usize>, max: usize)
    {
        *j += 1;
        rot_one(tab, max);
    }
    
    fn find_next_pivot(mat: &Matrix<f32>, i: &mut usize, j: &mut usize, tab: &mut Vec<usize>) -> f32
    where f32: CompareK,
    {
        let mut pivot = 0.;
        while *j < mat.n
        {
            while *i < mat.m
            {
                if mat.coor[*j].coor[*i].compare_f(0.) == 0
                {
                    pivot = mat.coor[*j].coor[*i];
                    inc_indice(j, tab, mat.n);
                    return pivot;
                }
                // rot_one()
                *i += 1;
            }
           inc_indice(j, tab, mat.n);
        }
        pivot
    }

    fn reduce_row(mat: &mut Matrix<f32>, j:usize, i:usize, column_reduce: &Vec<usize>, pivot: f32)
    {
        let mut line_pivot: Vector<f32>;
        let mut scal: f32;

        mat.coor[j - 1].scl(pivot.inv()); // pivot a 1
                
        for red in column_reduce
        {
            line_pivot = mat.coor[j- 1].clone();
            scal = -1. * mat.coor[*red].coor[i];
            line_pivot.scl(scal);
            mat.coor[*red].add(&line_pivot);
        }
    }

    impl Rowechelon<f32> for Matrix<f32>
    where f32: CompareK 
    + Add<Output = f32>
    + Debug
    + Sub
    + Mul
    + Copy
    + Clone
    + Sub<Output = f32>
    + Mul<Output = f32>
    + Inv
    {
        fn row_echelon(&mut self)
        {
            let mut column_reduce : Vec<_> = (0..self.n - 1).collect(); //[1, 2, 3] // a definir dans la fun next_pivot
            let mut pivot: f32;
            let mut i: usize = 0;
            let mut j: usize = 0;

            while i < self.m && j < self.n
            {
                pivot = find_next_pivot(&self, & mut i, & mut j, &mut column_reduce);
                if pivot == 0. { break; }
                reduce_row(self, j, i, &column_reduce, pivot);
            }
        }
    }


    fn move_on_row(dest: & mut Vec<usize>, src:  &Vec<usize>, indice_column: &usize)
    {
        let mut i_dest: usize = 0;
        for mut i_src in 0..src.len()
        {
            if *indice_column != i_src
            {
                dest[i_dest] = src[i_src];
                i_dest += 1;
            }
        }
    
    }

    fn fill_mat(mat_dest: &mut Matrix<f32>, mat_src: &Matrix<f32>, column_fill: &Vec<usize>, row_fill: &Vec<usize>)
    {
        let mut i_mat: usize = 0;
        let mut j_mat: usize = 0;

        for j in row_fill
        {
            for i in column_fill
            {
                mat_dest.coor[j_mat].coor[i_mat] = mat_src.coor[*j].coor[*i];
                i_mat += 1;
            }
            i_mat = 0;
            j_mat += 1;
        }
    }

    

    pub trait Determinant<K>{
        fn determinant2(&mut self) -> f32;
        fn determinant_n(&mut self, mat_one: &mut Matrix<f32>) -> f32;
        // fn determinant4(&mut self, mat_one: &mut Matrix<f32>) -> f32;
        fn determinant(&mut self) -> f32;
    }
    
    impl Determinant<f32> for Matrix<f32>
    where f32: CompareK 
    + Add<Output = f32>
    + Debug
    + Sub
    + Mul
    + Copy
    + Clone
    + Sub<Output = f32>
    + Mul<Output = f32>
    + Inv
    {
        fn determinant2(&mut self) -> f32
        {
            self.coor[0].coor[0] * self.coor[1].coor[1] - self.coor[1].coor[0] * self.coor[0].coor[1]
        }

        fn determinant_n(&mut self, mat_one : &mut Matrix<f32>) -> f32 {
            let mut column_fill: Vec<_> = (1..self.n).collect();
            let mut row_fill: Vec<_> = column_fill.clone();
            let mut dim: Vec<_> = (0..self.n).collect();
            let mut fact_pow: f32 = 0.;
            let mut det: f32 = 0.;

            for indice_column in 0..self.n
            {
                move_on_row(& mut column_fill, &dim, &indice_column);
                fill_mat(mat_one, &self, &column_fill, &row_fill);
                det += self.coor[0].coor[indice_column] * (-1.).pow(fact_pow) * mat_one.determinant();
                fact_pow += 1.;
            }
            det
        }

        fn determinant(&mut self) -> f32{
            if self.n != self.m {panic!("Matrix is not square");}
            match self.n {
                2 => self.determinant2(),
                3 => self.determinant_n(& mut Matrix::from(&[Vector::from(&[0.,0.]), Vector::from(&[0.,0.])])),
                4 => self.determinant_n(& mut Matrix::from(&[Vector::from(&[0.,0., 0.]), Vector::from(&[0.,0., 0.]), Vector::from(&[0.,0., 0.])])),
                _ => panic!("dimension of the matrix is too high")
            }
        }
    }
    
    fn vec_nul(n: usize) -> Vector<f32>
    {
        let mut vec_nul: Vector<f32> = Vector::new(n);
        vec_nul.coor = vec!(0.; n);
        vec_nul
    }

    fn mat_nul(n: usize, m:usize) -> Matrix<f32>
    {
        let mut mat: Matrix<f32> = Matrix::new_one(n, m);
        for i in 0..n
        {
            mat.coor.push(vec_nul(m)); 
        }
        mat
    }

    fn mat_id(n: usize) -> Matrix<f32>
    {
        let mut mat: Matrix<f32> = mat_nul(n, n);
        for i in 0..n
        {
           mat.coor[i].coor[i] = 1.;
        }
        mat
    }

    pub trait Inverse{
        fn inverse(&mut self) -> Matrix<f32>;
    }

    impl Inverse for Matrix<f32>
    {
        fn inverse(&mut self) -> Matrix<f32>
        {
            if self.n != self.m { panic!("Matrix isn't square!"); }
            else if self.determinant() == 0. { panic!("Matrix's determinant is nul: the matrix isn't inversible");} 
            let mut vec_index: Vec<_> = (0..self.n).collect();
            let mut vec_index2: Vec<_> = (self.n .. 2 * self.n).collect();
            let mut m: Matrix<f32> = mat_nul(self.n,2 * self.n);
            let mut m_id: Matrix<f32> = mat_id(self.n);
            
            if self.m != self.n {panic!("Matrix need to be square!")}
            
            m.copy(self, &vec_index, &vec_index, &vec_index, &vec_index);
            m.copy(&m_id, &vec_index, &vec_index2, &vec_index, &vec_index);
            m.row_echelon();
            self.copy(&m, &vec_index, &vec_index, &vec_index, &vec_index2);
            m_id
        }
    }

    fn check_vec_nul(v: &Vector<f32>) -> usize
    {
        for i in 0..v.m
        {
            if v.coor[i] != 0.
            {
                return 0
            }
        }
        1
    }

    fn calcul_rank(m: &Matrix<f32>) -> usize
    {
        let mut rank: usize = m.n;
        for i in 0..m.n
        {
            rank -= check_vec_nul(&m.coor[i]);
        }
        return rank
    }

    pub trait Rank{
        fn rank(&mut self) -> usize;
    }

    impl Rank for Matrix<f32>
    {
        fn rank(&mut self) -> usize
        {
            let mut m: Matrix<f32> = mat_nul(self.n,self.m);
            let mut vec_index: Vec<_> = (0..self.n).collect();
            let mut vec_index2: Vec<_> = (0 .. self.m).collect();
            m.copy(self, &vec_index, &vec_index, &vec_index, &vec_index);
            m.print();
            m.row_echelon();
            m.print();
            calcul_rank(&m)
        }
    }
}

