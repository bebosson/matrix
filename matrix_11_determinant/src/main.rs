use matrix_lib::matrix::Matrix as Matrix;
use vector::vector::Vector as Vector;
use matrix_lib::matrix::Determinant;

fn main() {


    let E0 = Vector::from(&[8., 5., -2.]);
    let E1 = Vector::from(&[4., 7., 20.]);
    let E2 = Vector::from(&[7., 6., 1.]);
    let mut E = Matrix::from(&[E0, E1, E2]);
    let F0 = Vector::from(&[2., 0., 0.]);
    let F1 = Vector::from(&[0., 2., 0.]);
    let F2 = Vector::from(&[0., 0., 2.]);
    // let mut E = Matrix::from(&[E0, E1, E2]);
    let mut F = Matrix::from(&[F0, F1, F2]);
    let C0 = Vector::from(&[5., 1., 2., 3.]);
    let C1 = Vector::from(&[4., 5., 1., 1.]);
    let C2 = Vector::from(&[2., 2., 4., 2.]);
    let C3 = Vector::from(&[5., 6., 1., 3.]);
    let mut C = Matrix::from(&[C0, C1, C2, C3]);

    let G0 = Vector::from(&[8., 5., -2., 4.]);
    let G1 = Vector::from(&[4., 2.5, 20., 4.]);
    let G2 = Vector::from(&[8., 5., 1., 4.]);
    let G3 = Vector::from(&[28., -4., 17., 1.]);
    let mut G = Matrix::from(&[G0, G1, G2, G3]);


    println!("{}", E.determinant());
    println!("{}", F.determinant());
    println!("{}", C.determinant());
    println!("{}", G.determinant());
}
