pub mod vector {

    use std::clone::Clone;
    use std::cmp::PartialOrd;
    use std::fmt::Debug;
    use std::ops::Add;
    use std::ops::Mul;
    use std::ops::Sub;
    use std::process::Output;

    // enum field<K> {
    //     f32(K),
    //     Complex(K),
    // }

    // enum vec_space<V, K>
    // {
    //     Matrix(V),
    //     Vector(K)
    // }

    pub struct Complex {
        pub real: f32,
        pub im: f32,
    }

    #[derive(Debug, Clone)]
    pub struct Vector<K>
    {
        pub coor: Vec<K>, // K est soit un f32 soit un Complex
        pub m: usize,     // taille du vecteur
    }

    impl<K, const A: usize> From<&[K; A]> for Vector<K>
    where
        K: Copy + Clone + Debug + Add<Output = K> + Sub<Output = K> + Mul<Output = K>,
    {
        fn from(v: &[K; A]) -> Self {
            Self {
                coor: v.to_vec(),
                m: v.len(),
            }
        }
    }


    impl<K> Vector<K>
    //traits
    where
        K: Copy + Clone + Debug + Add<Output = K> + Sub<Output = K> + Mul<Output = K>,
    {
        pub fn new(m: usize) -> Vector<K> {
            Vector {
                coor: Vec::<K>::with_capacity(m),
                m: m,
            }
        }

        pub fn print_all(&self) {
            print!("----------\n");
            print!("coor: {:?}\n", self.coor);
            print!("m: {:?}\n", self.m);
            print!("----------\n");
        }

        pub fn print(&self) {
            println!("{:?}", self.coor);
        }

        pub fn add(&mut self, v: &Vector<K>) {
            if self.m != v.m {
                panic!("undifined result!");
            } else {
                let mut i: usize = 0;
                while i < v.m {
                    self.coor[i] = self.coor[i] + v.coor[i];
                    i += 1;
                }
            }
        }
        pub fn sub(&mut self, v: &Vector<K>) {
            if self.m != v.m {
                panic!("undifined result!");
            } else {
                let mut i: usize = 0;
                while i < v.m {
                    self.coor[i] = self.coor[i] - v.coor[i];
                    i += 1;
                }
            }
        }

        pub fn scl(&mut self, a: K) {
            let mut i: usize = 0;
            while i < self.m {
                self.coor[i] = self.coor[i] * a;
                i += 1;
            }
        }
    }



    pub trait Norme {
        fn norme_1(&self) -> f32;
        fn norm(&self) -> f32;
        fn norm_inf(&self) -> f32;
    }
    
    pub trait Abs {
        fn abs(&self) -> f32;
    }
    
    pub trait Pow{
        fn pow(&self, u: f32) -> f32;
    }
    
    pub trait Max{
        fn my_max(&self, u: f32) -> f32;
    }
    
    impl Abs for f32 {
        fn abs(&self) -> f32 {
            if *self < 0.0 {
                *self * -1.
            } else {
                *self
            }
        }
    }
    
    impl Abs for Complex {
        fn abs(&self) -> f32 {
            if self.real.is_sign_negative(){
                self.real *-1.
            }
            else {
                self.real
            }
        }
    }
    
    impl Pow for f32{
        fn pow(&self, n: f32) -> f32 {
            self.powf(n)
        }
    }
    
    impl Max for f32{
        fn my_max(&self, n: f32) -> f32 {
            self.max(n)
        }
    }
    
    impl<K> Norme for Vector<K>
    where K: Max + Pow + Abs + Copy + Debug + Add<Output = K> + Sub<Output = K> + Mul<Output = K> + PartialEq + PartialOrd,
    {
        fn norme_1(&self) -> f32 {
            self.coor.iter().fold(0.,|num ,x| num + x.abs())
        }
    
        fn norm(&self) -> f32 {
            let a: f32;
            a = self.coor.iter().fold(0.,|num ,x| num + x.pow(2.));
            a.pow(0.5)
            //.fold(0.,|num ,x| num + x.pow(2.))
        }
    
    
        fn norm_inf(&self) -> f32 {
            self.coor.iter().fold(0.,|num ,x| x.abs().my_max(num))
        }
    }
    
    
    pub trait Dot<K>{
        fn dot(&self, v: &Vector<K>) -> K;
    }

    impl<K> Dot<K> for Vector<K>
    where K: Max + Pow + Abs + Copy + Debug + Add<Output = K> + Sub<Output = K> + Mul<Output = K> + PartialEq + PartialOrd,
    {
        fn dot(&self, v: &Vector<K>) -> K {
            let mut ret: K;
    
            if self.m != v.m || (v.m == 0) {
                panic!("incorrect input");
            }
            ret = self.coor[0] * v.coor[0];
            for i in 1..self.m {
                ret = ret + self.coor[i] * v.coor[i];
            }
            return ret;
        }   
    }
    
    pub fn min<K>(u: K, v: K) -> K
    where
        K: PartialEq + PartialOrd,
    {
        if u <= v {
            return u;
        } else {
            return v;
        }
    }

    pub fn max<K>(u: K, v: K) -> K
    where
        K: PartialEq + PartialOrd,
    {
        if u >= v {
            return u;
        } else {
            return v;
        }
    }

    pub trait Mymul{
        fn mymul(&self, u: f32) -> f32;
    }
    
    impl Mymul for f32{
        fn mymul(&self, u: f32) -> f32{
            *self * u
        }
    }
    
    
    

    pub trait Scl{
        fn scl(&mut self, a: f32);
    }

    impl Scl for Vector<f32>
    {
        fn scl(&mut self, a: f32){
            for i in 0..self.m
            {
                self.coor[i] = self.coor[i].mymul(a);
            }
        }
    }

    
}