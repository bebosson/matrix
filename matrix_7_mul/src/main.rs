
use matrix_lib::matrix::Matrix as Matrix;
use vector::vector::Vector as Vector;


fn main() {
    let A0 = Vector::from(&[1., 0., 1.]);
    let A1 = Vector::from(&[2., 1., 1.]);
    let A2 = Vector::from(&[0., 1., 1.]);
    let A3 = Vector::from(&[1., 1., 2.]);


    let B0 = Vector::from(&[1., 2., 1.]);
    let B1 = Vector::from(&[2., 3., 1.]);
    let B2 = Vector::from(&[4., 2., 2.]);
    let mut A =  Matrix::from(&[A0, A1, A2, A3]);
    let B =  Matrix::from(&[B0, B1, B2]);

    let C0 = Vector::from(&[1., 2., 3.]);
    let C1 = Vector::from(&[4., 5., 6.]);
    let C2 = Vector::from(&[7., 8., 9.]);
    let C3 = Vector::from(&[10., 11., 12.]);
    let mut C =  Matrix::from(&[C0, C1, C2, C3]);
    let D0 = Vector::from(&[2., 7.]);
    let D1 = Vector::from(&[1., 2.]);
    let D2 = Vector::from(&[3., 6.]);
    let V = Vector::from(&[1.,0.]);
    let D =  Matrix::from(&[D0, D1, D2]);
    let R0 = A.mul_mat(B);
    let R1 = C.mul_mat(D);
    let D0 = Vector::from(&[2., 7.]);
    let D1 = Vector::from(&[1., 2.]);
    let D2 = Vector::from(&[3., 6.]);
    let V = Vector::from(&[1.,2.,3.]);
    let F0 = Vector::from(&[1., 2., 3.,2.]);
    let F1 = Vector::from(&[4., 5., 6.,3.]);
    let mut F =  Matrix::from(&[F0, F1]);
    let E0 = Vector::from(&[1., 2., 3.]);
    let E1 = Vector::from(&[4., 5., 6.]);
    let mut E =  Matrix::from(&[E0, E1]);

    let mut D =  Matrix::from(&[D0, D1]);
    // let W1 = F.mul_mat(E); // pas defini (2x4 X 2x3)

    let R2 = D.mul_mat(F);

    R0.print();
    R1.print();
    R2.print();
}
