use std::clone::Clone;
use std::fmt::Debug;
use std::ops::Add;
use std::ops::Sub;
use std::ops::Mul;
use std::cmp::PartialOrd;
use vector::vector::Vector as Vector;
use matrix_lib::matrix::Matrix as Matrix;



trait Linearinterpolation<V,K> 
    where K: PartialEq + PartialOrd,
{
    fn lerp(u: V, v: V, t: K) -> V;
}


pub fn lerp<K>(u: K, v: K, t: K) -> K
where K: Debug + Copy + Add<Output = K> + Sub<Output = K> + Mul<Output = K> + PartialEq + PartialOrd,
{
    let min: K = vector::vector::min(u, v);
    let max: K = vector::vector::max(u, v);
    let ret = (max - min) *  t + min;

    return ret;   
}

impl<K> Linearinterpolation<Vector<K>,K> for Vector<K>
where K: Copy + Debug + Add<Output = K> + Sub<Output = K> + Mul<Output = K> + PartialEq + PartialOrd,
{
    fn lerp(u: Vector<K>, v: Vector<K>, t: K) -> Vector<K>
    {
        let mut ret = Vector::new(u.m);
        let mut i = 0;

        while i < u.m
        {
            ret.coor.push(lerp(u.coor[i], v.coor[i], t));
            i += 1;
        }
        return ret;
    }
}

impl<K> Linearinterpolation<Matrix<K>,K> for Matrix<K>
where K: Copy + Debug + Add<Output = K> + Sub<Output = K> + Mul<Output = K> + PartialEq + PartialOrd,
{
    fn lerp(u: Matrix<K>, v: Matrix<K>, t: K) -> Matrix<K>
    {
        let mut ret = Matrix::new(u.m, u.n);

        for i in 0..u.m
        {
            for j in 0..u.n
            {
                ret.coor[i].coor.push(lerp(u.coor[i].coor[j], v.coor[i].coor[j], t));
            }
        }
        return ret;
    }
}


fn main()
{
    let v1 = Vector::from(&[1.,2.]);
    let v2 = Vector::from(&[2.,3.]);
    let v3 = Vector::from(&[2.,1.]);
    let v4 = Vector::from(&[3.,4.]);
    let v5 = Vector::from(&[2.,1.]);
    let v6 = Vector::from(&[20.,10.]);
    let v7 = Vector::from(&[30.,40.]);

    println!("{}", lerp(21., 42., 0.3));
    println!("{}", lerp(0., 1., 0.));
    println!("{}", lerp(0., 1., 1.));
    println!("{}", lerp(0., 1., 0.5));
    Vector::lerp(Vector::from(&[2., 1.]), Vector::from(&[4., 2.]), 0.3).print();
    // Matrix::lerp(Matrix::from(&[v3, v4]), Matrix::from(&[v6, v7]), 0.5).print();
    // Matrix::lerp(Matrix::from(&[v3, v4]), Matrix::from(&[v6, v7]), 0.5).print();
    println!("{:?}", Matrix::lerp(Matrix::from(&[v3, v4]), Matrix::from(&[v6, v7]), 0.5));
}