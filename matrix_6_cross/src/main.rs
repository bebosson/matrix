use vector::vector::Vector as Vector;
use std::fmt::Debug;
use std::ops::Add;
use std::ops::Mul;
use std::ops::Sub;

fn cross_product<K>(u: &Vector<K>, v: &Vector<K>) -> Vector<K>
where K: Copy + Clone + Debug + Add<Output = K> + Sub<Output = K> + Mul<Output = K> + Debug,
{
    if u.m != v.m || (v.m < 3) {
        panic!("incorrect input");
    }
    let mut w = Vector::new(3);
    w.coor.push(u.coor[1] * v.coor[2] - u.coor[2] * v.coor[1]);
    w.coor.push(u.coor[2] * v.coor[0] - u.coor[0] * v.coor[2]);
    w.coor.push(u.coor[0] * v.coor[1] - u.coor[1] * v.coor[0]);
    w
}

fn main(){
    let u0 = Vector::from(&[0., 0., 1.]);
    let v0 = Vector::from(&[1., 0., 0.]);
    cross_product(&u0, &v0).print();
    let u1 = Vector::from(&[1., 2., 3.]);
    let v1 = Vector::from(&[4., 5., 6.]);
    cross_product(&u1, &v1).print();
    let u2 = Vector::from(&[4., 2., -3.]);
    let v2 = Vector::from(&[-2., -5., 16.]);
    cross_product(&u2, &v2).print();
}