use vector::vector::{Vector, Dot, Norme, Max, Abs, Pow};
use std::fmt::Debug;
use std::ops::Add;
use std::ops::Mul;
use std::ops::Sub;

pub trait Mydiv{
    fn mydiv(&self, u: f32) -> f32;
}

impl Mydiv for f32{
    fn mydiv(&self, u: f32) -> f32{
        if u == 0. { panic!("div by zero!")}
        *self / u
    }
}


fn cos<K>(u: &Vector<K>, v: &Vector<K>) -> f32
    where K: Max + Pow + Abs + Copy + Debug + Add<Output = K> + Sub<Output = K> + Mul<Output = K> + Mydiv + PartialEq + PartialOrd,
{
    u.dot(&v).mydiv(u.norm() * v.norm())
}

fn main() {
    let u = Vector::from(&[0., 5.]);
    let v1 = Vector::from(&[5., 0.]);
    
    println!("{}",cos(&u,&v1));
}