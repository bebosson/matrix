use std::clone::Clone;
use std::marker::Copy;
use std::fmt::Debug;
use std::ops::Add;
use std::ops::Sub;
use std::ops::Mul;

pub use vector::vector::Vector;

fn linear_combination<K>(u: &[Vector<K>], coefs: &[K]) -> Vector<K>
where K: Copy + Clone + Debug + Add<Output = K> + Sub<Output = K> + Mul<Output = K>,
{

    let mut v = Vector::new(u[0].coor.len());

    if u.len() != coefs.len()
    {
        panic!("array has not the same size :/ ");
    }
    if u.len() > 1
    {
        {
            let mut i:usize = 1;
            while i < u.len()
            {
                if u[i].m != u[i-1].m
                {
                    panic!("origin vector matrix needs to have the same dimension")
                }
                i += 1;
            }
        }
    }

    for j in 0..u[0].m
    {
        for i in 0..u.len()
        {
            if i == 0
            {
                v.coor.push(u[i].coor[j] * coefs[i]);
            }
            else
            { 
                v.coor[j] = v.coor[j] +  u[i].coor[j] * coefs[i];
            }
            v.print(); 
        }
    }
    return v;
}

fn main()
{
    let e1 = Vector::from(&[1., 1., 0.]);
    let e2 = Vector::from(&[0., 1., 1.]);
    let e3 = Vector::from(&[0., 0., 1.]);
    let v1 = Vector::from(&[1., 2., 3.]);
    let v2 = Vector::from(&[0., 10., -100.]);

    
 
    linear_combination(&[e1,e2,e3], &[1.,2.,3.]).print();
    linear_combination(&[v1,v2], &[10.,-2.]).print();

}