use matrix_lib::matrix::Matrix as Matrix;
use vector::vector::Vector as Vector;

fn main() {
    let A0 = Vector::from(&[1., 0., 1.]);
    let A1 = Vector::from(&[2., 1., 1.]);
    let A2 = Vector::from(&[0., 3., 1.]);
    let F0 = Vector::from(&[1., 2., 3.,2.]);
    let F1 = Vector::from(&[1., 2., 3.,2.]);
    let F2 = Vector::from(&[1., 2., 3.,2.]);
    let F3 = Vector::from(&[1., 2., 3.,2.]);
    let mut A = Matrix::from(&[A0, A1, A2]);
    A.print();
    A.transpose().print();
}
