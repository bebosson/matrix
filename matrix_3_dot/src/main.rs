use vector::vector::Vector as Vector;
use vector::vector::Dot;

fn main()
{
    let u = Vector::from(&[0., 2.]);
    let v = Vector::from(&[2., 0.]);
    println!("{}", u.dot(&v));
    let u = Vector::from(&[1., 1.]);
    let v = Vector::from(&[1., 1.]);
    println!("{}", u.dot(&v));
    let u = Vector::from(&[3.,2.]);
    let v = Vector::from(&[3., 2.,]);
    println!("{}", u.dot(&v));
}